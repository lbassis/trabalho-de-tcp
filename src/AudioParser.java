import java.awt.TextField;
import java.util.Random;

public class AudioParser {
	
	private static final String TRACK_CODE = "V";
	
	private static final char LA = 'A';
	private static final char TI = 'B';
	private static final char DO = 'C';
	private static final char RE = 'D';
	private static final char MI = 'E';
	private static final char FA = 'F';
	private static final char SOL = 'G';

	private static final String BLANK = "";
	private static final char SPACE = ' ';
	private static final char REST_CODE = ' ';
	private static final char INSTRUMENT_CODE = '\n';
	private static final char DEC_BPM_CODE = ';';
	private static final char INC_BPM_CODE = ',';
	private static final char INC_VOLUME_CODE = '!';
	private static final char DEC_VOLUME_CODE = '0'; 
	private static final char DEFAULT_OCTAVE_CODE1 = '.';
	private static final char DEFAULT_OCTAVE_CODE2 = '?';
	
	private static final int FIRST_ELEMENT = 0;
	
	private static final String INSTRUMENT_STRING = "I";
	private static final int FIRST_INSTRUMENT = 0;
	private static final int LAST_INSTRUMENT = 127;
	
	private static final String TEMPO_STRING = "T";
	private static final int DEFAULT_TEMPO = 170;
	private static final int TEMPO_INCREMENT = 15;
	private static final int MIN_TEMPO = 50;
	private static final int MAX_TEMPO = 290;
	
	private static final String REST_STRING = "R";
	
	private static final int DEFAULT_VOLUME = 12500;
	private static final int VOLUME_INCREMENT = 500;
	private static final String VOLUME_STRING = "X[Volume]=";
	
	private static final int DEFAULT_OCTAVE = 5;
	private static final int OCTAVE_INCREMENT = 1;
	
	private static final int MIN_VOLUME = 10200;
	private static final int MAX_VOLUME = 16383;
	private static final int FIRST_OCTAVE = 0;
	private static final int LAST_OCTAVE = 9;
	private static final char FIRST_NUMBER_ASCII = 48;
	private static final char LAST_NUMBER_ASCII = 57;
	

	
	private String text;
	private int currentInstrument = 0;
	private int currentTempo;
	private int currentVolume;
	private int currentOctave;
	
	//	initialization
	public AudioParser (String text, int instrument) {		
		setCurrentInstrument(instrument);
		setText(parse(text));
	}	
	public AudioParser (TextField texts[], int instruments[], int tracks) {
		
		String resultingText = new String(BLANK);
		int i = 0;
		
		while (i<tracks) {
			setCurrentInstrument(instruments[i]);
			resultingText += TRACK_CODE + String.valueOf(i) + SPACE + parse(texts[i].getText());
			i++;
		}
		setText(resultingText);
	}
	private void setDefaults() {
		setCurrentTempo(DEFAULT_TEMPO);
		setCurrentVolume(DEFAULT_VOLUME);
		setCurrentOctave(DEFAULT_OCTAVE);
	}

	//	text handling
	private String insertStringHeader(String text) {
		
		String newText = new String();
		newText = INSTRUMENT_STRING + currentInstrument + SPACE + TEMPO_STRING + DEFAULT_TEMPO + SPACE + text;
		
		return newText;
	}
	private String handleText(String text) {
		
		int i = 0;
		
		while (i<text.length()) {

			char character = text.charAt(i);
			String parsedCode = new String();
			switch (character) {
			
			case INSTRUMENT_CODE:
				parsedCode = changeInstrument();
				break;
			case INC_BPM_CODE:
				parsedCode = incBPM();
				break;
			case DEC_BPM_CODE:
				parsedCode = decBPM();
				break;
			case INC_VOLUME_CODE:
				parsedCode = incVolume();
				break;
			case DEC_VOLUME_CODE:
				parsedCode = decVolume();
				break;
			case REST_CODE:
				parsedCode = REST_STRING + SPACE;
				break;
			case DEFAULT_OCTAVE_CODE1:
			case DEFAULT_OCTAVE_CODE2:
				returnDefaultOctave();
				parsedCode = BLANK;
				break;
			case LA:
			case TI:
			case DO:
			case RE:
			case MI:
			case FA:
			case SOL:
				parsedCode = String.valueOf(character) + String.valueOf(getCurrentOctave()) + SPACE;
				break;
			default:
				if (isConsonant(character)) {
					incOctave();
					parsedCode = BLANK;
				}
				else if (isVowel(character)) {
					decOctave();
					parsedCode = BLANK;
				}
				else if (isNumber(character)) {
					if (isEven(character)) {
						setCurrentTempo(DEFAULT_TEMPO);
						parsedCode = BLANK;
					}
					else {
						setCurrentOctave(DEFAULT_OCTAVE);
						parsedCode = BLANK;
					}
				}
				
				else	
					parsedCode = BLANK;
				break;
			}
			
			text = appendParsedText(i, parsedCode, text);
			i = i + parsedCode.length();

		}
		
		return text;
	}	
	private String appendParsedText(int i,String parsedText, String text) {
		
		String appendedText = new String();
		
		if (i == FIRST_ELEMENT) {
			appendedText = parsedText+text.substring(FIRST_ELEMENT+1);
		}
		
		else {
			appendedText = text.substring(FIRST_ELEMENT, i) + parsedText + text.substring(i+1);
		}
		
		return appendedText;
	}
	private String parse(String text) {
		
		setDefaults();
		text = text.toUpperCase();
		text = handleText(text);
		text = insertStringHeader(text);
		
		return text;
	}
	
	// boolean expressions
	private Boolean isConsonant(char character) {
		
		char consonant[] = {'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'};
		
		int i;
		
		for (i=0; i<consonant.length; i++) {
			if (consonant[i] == character) {
				return true;
			}
		}
		return false;
	}
	private Boolean isVowel(char character) {
		
		char consonant[] = {'I', 'O', 'U'};
		
		int i;
		
		for (i=0; i<consonant.length; i++) {
			if (consonant[i] == character) {
				return true;
			}
		}
		return false;
	}
	private Boolean isNumber(char character) {
		
		if (character >= FIRST_NUMBER_ASCII && character <= LAST_NUMBER_ASCII) {
			return true;
		}
		
		else return false;
	}
	private Boolean isEven(char character) {
		
		if (isNumber(character)) {
			if (character % 2 == 0) {
				return true;
			}
			
			else return false;
		}
		else return false;
	}

	//	code transformation
	private String changeInstrument() {
		
		Random ran = new Random();
		int newInstrument;
		
		do {
			newInstrument = ran.nextInt(LAST_INSTRUMENT)+FIRST_INSTRUMENT;
		}while (newInstrument == getCurrentInstrument());
		
		setCurrentInstrument(newInstrument);
		
		String instrumentString = SPACE + INSTRUMENT_STRING + String.valueOf(newInstrument) + SPACE;
		
		return instrumentString;
	}	
	private String incBPM() {
		
		int newTempo = (int) Math.ceil(getCurrentTempo() + TEMPO_INCREMENT);
		
		if (newTempo <= MAX_TEMPO) {
			setCurrentTempo(newTempo);
		}
		else {
			newTempo = MAX_TEMPO;
			setCurrentTempo(MAX_TEMPO);
		}
		
		String tempoString = SPACE + TEMPO_STRING + String.valueOf(newTempo) + SPACE;
		
		return tempoString;
	}
	private String decBPM() {
		
		int newTempo = (int) Math.ceil(currentTempo - TEMPO_INCREMENT);
		
		if (newTempo >= MIN_TEMPO) {
			setCurrentTempo(newTempo);
		}
		else {
			newTempo = MIN_TEMPO;
			setCurrentTempo(MIN_TEMPO);
		}
		
		String tempoCode = SPACE + TEMPO_STRING + String.valueOf(newTempo) + SPACE;
		
		return tempoCode;
	}
	private String incVolume() {
		
		int newVolume = getCurrentVolume() + VOLUME_INCREMENT;
		
		if(newVolume <= MAX_VOLUME) {
			setCurrentVolume(newVolume);
		}
		else {
			newVolume = MAX_VOLUME;
			setCurrentVolume(MAX_VOLUME);
		}
		
		String volumeString = SPACE + VOLUME_STRING + String.valueOf(newVolume) + SPACE;
		
		return volumeString;
	}	
	private String decVolume() {
		
		int newVolume = getCurrentVolume() - VOLUME_INCREMENT;
		
		if (newVolume >= MIN_VOLUME) {
			setCurrentVolume(newVolume);
		}	
		else {
			newVolume = MIN_VOLUME;
			setCurrentVolume(MIN_VOLUME);
		}
		
		String volumeString = SPACE + VOLUME_STRING + String.valueOf(newVolume) + SPACE;
		
		return volumeString;
	}
	private void incOctave() {
		
		int newOctave = getCurrentOctave() + OCTAVE_INCREMENT;
		
		if (newOctave <= LAST_OCTAVE) {
			setCurrentOctave(newOctave);
		}
		else {
			setCurrentOctave(DEFAULT_OCTAVE);
		}
	}
	private void decOctave() {
		
		int newOctave = getCurrentOctave() - OCTAVE_INCREMENT;
		
		if (newOctave >= FIRST_OCTAVE) {
		setCurrentOctave(newOctave);
		}
		else {
			setCurrentOctave(DEFAULT_OCTAVE);
		}
	}
	private void returnDefaultOctave() {
		setCurrentOctave(DEFAULT_OCTAVE);
	}

	//	gets/sets
	public String getText() {
		return this.text;
	}
	private void setText(String newText) {
		this.text = newText;
	}
	private int getCurrentInstrument() {
		return this.currentInstrument;
	}
	private void setCurrentInstrument(int instrument) {
		this.currentInstrument = instrument;
	}
	private int getCurrentTempo() {
		return this.currentTempo;
	}
	private void setCurrentTempo(int tempo) {
		this.currentTempo = tempo;
	}
	private int getCurrentVolume() {
		return this.currentVolume;
	}
	private void setCurrentVolume(int volume) {
		this.currentVolume = volume;
	}
	private int getCurrentOctave() {
		return this.currentOctave;
	}
	private void setCurrentOctave(int octave) {
		this.currentOctave = octave;
	}
}
	