import java.applet.Applet;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import org.jfugue.Player;


@SuppressWarnings("serial") // queria que eu serializasse a classe????


public class Interface extends Applet implements ActionListener {

	private static final int WINDOW_WIDTH = 400;
//	private static final int WINDOW_HEIGHT = 400;
	
	private static final int TEXT_DESCRIPTION_X = 30;
	private static final int TEXT_DESCRIPTION_Y = 10;
	
	private static final String INSTRUMENT_STRING = "I";
	private static final int FIRST_INSTRUMENT = 0;
	private static final int LAST_INSTRUMENT = 127;
	private static final int MAIN_INSTRUMENTS = 0;
	private static final int SYNTH_INSTRUMENTS = 81;
	private static final int PERCURSSIVE_INSTRUMENTS = 116;
	private static final String EXAMPLE_CHORD = " A";

	
	private static final int TRACKS = 3;
	private static final int TEXT_FIELD_X = 50;
	private static final int FIRST_TEXT_FIELD_Y = 50;
	private static final int TEXT_FIELD_WIDTH = 250;
	private static final int TEXT_FIELD_HEIGHT = 25;
	private static final int BUTTON_WIDTH = 50;
	private static final int BUTTON_HEIGHT = 25;
	private static final int MINI_BUTTON_WIDTH = 20;
	private static final int LABEL_WIDTH = 350;
	private static final int MINI_GAP = 3;
	private static final int GAP = 10;
	private static final int EXTRA_GAP = 45;
	
	private static final String EXTENSION = ".txt";
	private static final String FILE_DELIMITER = "\\A";
	
	private Label fieldsDescription = new Label("Write on the boxes to generate up to three tracks of music");
	private TextField[] texts = new TextField[TRACKS];
	private Button[] readButtons = new Button[TRACKS];
	private Button[] prevInstrumentButtons = new Button[TRACKS];
	private Button[] nextInstrumentButtons = new Button[TRACKS];
	private Label[] instrumentMessages = new Label[TRACKS];
	private int[] currentInstruments = new int[3];
	private Button playAllButton = new Button("Play All");
	private Button saveAllButton = new Button("Save All");
	private Label saveAllErrorLabel = new Label("Failed to create the file");
	
	private Label fileReadDescription = new Label("Or write a txt filename (without extension) on the following field");
	private TextField fileNameField = new TextField();
	private Button playFromFileButton = new Button("Play");
	private Button saveFromFileButton = new Button("Save");
	private Label fileErrorLabel = new Label();
	
	private String lastText = new String("");
	private String lastParsedText = new String("");
	
	//	assets management
	public void init() {
		
		setLayout(null);
		
		fieldsDescription.setBounds(TEXT_DESCRIPTION_X,TEXT_DESCRIPTION_Y, LABEL_WIDTH, BUTTON_HEIGHT);
		add(fieldsDescription);
		
		int i = 0;
		for (i=0; i<TRACKS; i++) {
			texts[i] = new TextField("Insert your text here", TEXT_FIELD_WIDTH);
			texts[i].setBounds(TEXT_FIELD_X, FIRST_TEXT_FIELD_Y + i*(BUTTON_HEIGHT + EXTRA_GAP), TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT);
			add(texts[i]);
			
			readButtons[i] = new Button ("Play");
			readButtons[i].setBounds(TEXT_FIELD_X + TEXT_FIELD_WIDTH + GAP, texts[i].getY(), BUTTON_WIDTH, BUTTON_HEIGHT);
			readButtons[i].addActionListener(this);
			add(readButtons[i]);
			
			prevInstrumentButtons[i] = new Button ("<");
			prevInstrumentButtons[i].setBounds(TEXT_FIELD_X, FIRST_TEXT_FIELD_Y + MINI_GAP + BUTTON_HEIGHT + i*(BUTTON_HEIGHT + EXTRA_GAP), MINI_BUTTON_WIDTH, BUTTON_HEIGHT);
			prevInstrumentButtons[i].addActionListener(this);
			add(prevInstrumentButtons[i]);
			
			nextInstrumentButtons[i] = new Button (">");
			nextInstrumentButtons[i].setBounds(prevInstrumentButtons[i].getX() + MINI_GAP + MINI_BUTTON_WIDTH, prevInstrumentButtons[i].getY(), MINI_BUTTON_WIDTH, BUTTON_HEIGHT);
			nextInstrumentButtons[i].addActionListener(this);
			add(nextInstrumentButtons[i]);
			
			instrumentMessages[i] = new Label("Use the arrows to select an instrument");
			instrumentMessages[i].setBounds(nextInstrumentButtons[i].getX() + MINI_BUTTON_WIDTH + MINI_GAP, prevInstrumentButtons[i].getY(), LABEL_WIDTH, BUTTON_HEIGHT);
			add(instrumentMessages[i]);
			
		}

		setCurrentInstruments(MAIN_INSTRUMENTS, SYNTH_INSTRUMENTS, PERCURSSIVE_INSTRUMENTS);
		
        playAllButton.setBounds((WINDOW_WIDTH - BUTTON_WIDTH)/2 - BUTTON_WIDTH/2 - MINI_GAP, instrumentMessages[TRACKS-1].getY() + instrumentMessages[TRACKS-1].getHeight() + GAP, BUTTON_WIDTH, BUTTON_HEIGHT);
        playAllButton.addActionListener(this);
        add(playAllButton);
        
        saveAllButton.setBounds(playAllButton.getX() + BUTTON_WIDTH + MINI_GAP, playAllButton.getY(), BUTTON_WIDTH, BUTTON_HEIGHT);
        saveAllButton.addActionListener(this);
        add(saveAllButton);
        
        saveAllErrorLabel.setBounds(saveAllButton.getX() + BUTTON_WIDTH + MINI_GAP, saveAllButton.getY(), LABEL_WIDTH, BUTTON_HEIGHT);
        saveAllErrorLabel.setVisible(false);
        add(saveAllErrorLabel);
        
        fileReadDescription.setBounds(fieldsDescription.getX(), saveAllErrorLabel.getY() + BUTTON_HEIGHT + EXTRA_GAP, LABEL_WIDTH, BUTTON_HEIGHT);
        add(fileReadDescription);
        
        fileNameField.setBounds(fileReadDescription.getX(), fileReadDescription.getY() + BUTTON_HEIGHT + MINI_GAP, TEXT_FIELD_WIDTH, BUTTON_HEIGHT);
        add(fileNameField);
        
        playFromFileButton.setBounds(fileNameField.getX() + TEXT_FIELD_WIDTH + GAP, fileNameField.getY(), BUTTON_WIDTH, BUTTON_HEIGHT);
        playFromFileButton.addActionListener(this);
        add(playFromFileButton);
        
        saveFromFileButton.setBounds(playFromFileButton.getX() + BUTTON_WIDTH + MINI_GAP, playFromFileButton.getY(), BUTTON_WIDTH, BUTTON_HEIGHT);
        saveFromFileButton.addActionListener(this);
        add(saveFromFileButton);
        
        fileErrorLabel.setBounds(saveAllErrorLabel.getX(), fileNameField.getY() + BUTTON_HEIGHT + MINI_GAP, LABEL_WIDTH, BUTTON_HEIGHT);
        fileErrorLabel.setVisible(false);
        add(fileErrorLabel);
	}

	private void hideErrorLabels() {
		saveAllErrorLabel.setVisible(false);
		fileErrorLabel.setVisible(false);
	}

	// action management
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource().equals(playAllButton)) {
			hideErrorLabels();
			Player player = new Player();
			AudioParser parsed = new AudioParser(texts, getCurrentInstruments(), TRACKS);
			
			String parsedText = parsed.getText();
			player.play(parsedText);
		}
		
		else if (e.getSource().equals(saveAllButton)) {
			try {
				Player player = new Player();
				AudioParser parsed = new AudioParser(texts, getCurrentInstruments(), TRACKS);
				
				String parsedText = parsed.getText();
				player.saveMidi(parsedText, new File("new song.mid"));
				hideErrorLabels();
			} catch (IOException exception) {
				saveAllErrorLabel.setVisible(true);
			}
		}
		
		else if (e.getSource().equals(playFromFileButton)) {
			
			try {
				File file = new File(fileNameField.getText() + EXTENSION);
				
				Scanner scanner;
				scanner = new Scanner(file);
				String textFromFile = scanner.useDelimiter(FILE_DELIMITER).next();
				scanner.close();
				
				String parsedText = new String();
				

				hideErrorLabels();
				if (textFromFile.equals(getLastText())) {
					parsedText = getLastParsedText();
				}
				
				else {
					AudioParser parsed = new AudioParser (textFromFile, FIRST_INSTRUMENT);
					parsedText = parsed.getText();
					
					setLastText(textFromFile);
					setLastParsedText(parsedText);
				}
				
				Player player = new Player();
				player.play(parsedText);

			} 
			
			catch (FileNotFoundException ex) {
				fileErrorLabel.setText("Failed to open file");
				fileErrorLabel.setVisible(true);
			}

		}

		else if (e.getSource().equals(saveFromFileButton)) {
			
			Player player = new Player();
			
			if (getLastParsedText().isEmpty()){
				fileErrorLabel.setText("No file to save");
				fileErrorLabel.setVisible(true);

			}
			else {
				try {
					player.saveMidi(getLastParsedText(), new File("new song.mid"));
				} catch (IOException e1) {
					fileErrorLabel.setText("Failed to save file");
					fileErrorLabel.setVisible(true);
				}
				hideErrorLabels();
			}
		}
		
		int i;
		for (i=0; i<TRACKS; i++) {
			if (e.getSource().equals(readButtons[i])) {
				Player player = new Player();
				AudioParser parsed = new AudioParser(texts[i].getText(), getCurrentInstruments()[i]);
				hideErrorLabels();

				String parsedText = parsed.getText();
				player.play(parsedText);
			}
			
			else if (e.getSource().equals(prevInstrumentButtons[i])) {
				hideErrorLabels();
				previousInstrument(i);
			}
			
			else if (e.getSource().equals(nextInstrumentButtons[i])) {
				hideErrorLabels();
				nextInstrument(i);
			}
		}
		
	}
	
	//	instruments management
	private void previewInstrument(int i) {
		Player player = new Player();
		player.play(INSTRUMENT_STRING + Integer.toString(getCurrentInstruments()[i]) + EXAMPLE_CHORD);
	}	
	private void previousInstrument(int i) {

		if (!isFirstInstrument(getCurrentInstruments()[i])) {
			instrumentMessages[i].setText("Use the arrows to select an instrument");
			getCurrentInstruments()[i]--;
			previewInstrument(i);
		}
		
		else {
			instrumentMessages[i].setText("First instrument reached");
			previewInstrument(i);
		}
		
	}
	private void nextInstrument(int i) {
		
		if (!isLastInstrument(getCurrentInstruments()[i])) {
			instrumentMessages[i].setText("Use the arrows to select an instrument");
			getCurrentInstruments()[i]++;
			previewInstrument(i);
		}
		
		else {
			instrumentMessages[i].setText("Last instrument reached");
			previewInstrument(i);
		}
		
	}
	
	//	boolean expressions
	private boolean isFirstInstrument(int instrument) {
		
		if (instrument == FIRST_INSTRUMENT) {
			return true;
		}
		
		else
			return false;
	}
	private boolean isLastInstrument(int instrument) {
		
		if (instrument == LAST_INSTRUMENT) {
			return true;
		}
		
		else 
			return false;
	}

	
	//	gets/sets
	public String getLastText() {
		return lastText;
	}
	private void setLastText(String lastText) {
		this.lastText = lastText;
	}
	private String getLastParsedText() {
		return lastParsedText;
	}
	private void setLastParsedText(String lastParsedText) {
		this.lastParsedText = lastParsedText;
	}
	private int[] getCurrentInstruments() {
		return currentInstruments;
	}
	private void setCurrentInstruments(int main, int synth, int percurssive) {
        this.currentInstruments[0] = main;
        this.currentInstruments[1] = synth;
        this.currentInstruments[2] = percurssive;
		
	}
	


}
